using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class SpwannerController : MonoBehaviour
{
    public Transform[] spawnPoints;
    public GameObject enemy;
    public float startTimeBtwSpwans;
    float timeBtwSpawns = 2;

    private void Start()
    {
        timeBtwSpawns = startTimeBtwSpwans;
    }
    private void Update()
    {
        if (PhotonNetwork.IsMasterClient == false || PhotonNetwork.CurrentRoom.PlayerCount != 2)
           return;

        if (timeBtwSpawns <= 0)
        {
            Vector3 spawPosition = spawnPoints[Random.Range(0, spawnPoints.Length)].position;
            PhotonNetwork.Instantiate(enemy.name, spawPosition, Quaternion.identity);
            timeBtwSpawns = startTimeBtwSpwans;
        }
        else
        {
            timeBtwSpawns -= Time.deltaTime;
        }
    }
}
