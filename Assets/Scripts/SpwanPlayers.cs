using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class SpwanPlayers : MonoBehaviour
{
    public GameObject player;
    public float minX, minY, maxX, Maxy;

    private void Start()
    {
        Vector2 randomPosition = new Vector2(Random.Range(minX, maxX), Random.Range(minY, Maxy));
        PhotonNetwork.Instantiate(player.name, randomPosition, Quaternion.identity);
    }
    
}
