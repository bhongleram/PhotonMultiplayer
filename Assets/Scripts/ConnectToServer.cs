using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;
public class ConnectToServer : MonoBehaviourPunCallbacks
{
    
    void Start()
    {
        //Connect To the Server 
        PhotonNetwork.ConnectUsingSettings();
    }

    //When Player gets connect to server 
    public override void OnConnectedToMaster()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
